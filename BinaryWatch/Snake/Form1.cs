﻿using System;
using System.Drawing;
using System.Windows.Forms;
//using System.Windows.Thickness;

namespace Snake
{
    public partial class Form1 : Form
    {
        
        Random rand = new Random();
        int id_snake = 2;
        bool apple = true;

        int moveX = new Random().Next(20); // column
        int moveY = new Random().Next(20); // row

        /// <summary>
        /// up = 0 | down = 1 | left = 2 | right = 3
        /// </summary>
        int direction = 3;

        public Form1()
        {
            InitializeComponent();
            DefaultPosition(snake1);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            MoveSnake(snake1, moveX, moveY);
            //if(apple)
            //    CreateApple();
        }

        /// <summary>
        /// Tworzy jabłko dla węża
        /// </summary>
        private void CreateApple()
        {
            bool b;
            apple = false;
            int x, y;
            do
            {
                x = rand.Next(20);
                y = rand.Next(20);
                try
                {
                    b = field.GetControlFromPosition(x, y).HasChildren;
                }
                catch (NullReferenceException)
                {
                    b = false;
                }
            } while (b);
            CreateNewSquare("apple", x, y); 
        }


        //private void EatApple()
        //{
        //    switch (direction)
        //    {
        //        case 0:
        //            if (field.GetControlFromPosition)
        //            {

        //            }
        //    }
        //}


        /// <summary>
        /// Ustawia punkty, po których mają się poruszać komórki
        /// </summary>
        private void SetPositionCell()
        {
            switch (direction)
            {
                case 0:
                    moveX = GetPositionCell(true);
                    moveY = GetPositionCell(false) - 1;
                    break;
                case 1:
                    moveX = GetPositionCell(true);
                    moveY = GetPositionCell(false) + 1;
                    break;
                case 2:
                    moveX = GetPositionCell(true) - 1;
                    moveY = GetPositionCell(false);
                    break;
                case 3:
                    moveX = GetPositionCell(true) + 1;
                    moveY = GetPositionCell(false);
                    break;
            }
        }

        /// <summary>
        /// Tworzy nowy kwadrat w polu 
        /// </summary>
        private void CreateNewSquare(string nameObj, int x, int y)
        {
            
            nameObj = nameObj == "apple" ? "apple" : "snake" + id_snake++;
            field.Controls.Add(new PictureBox
            {
                Name = nameObj,
                //Margin = 
                Dock = DockStyle.Fill,
                Size = new Size(20, 20),
                BackColor = Color.Red
            }, x, y);
        }

        /// <summary>
        /// Ustawia pozycje węża wzgledem wartosci directory
        /// </summary>
        private void MoveSnake(PictureBox square, int column, int row)
        {
            SetPositionCell();
            field.SetCellPosition(square, new TableLayoutPanelCellPosition(column, row));
        }

        /// <summary>
        /// Ustawia domyślna pozycje w elementu w polu
        /// </summary>
        private void DefaultPosition(PictureBox image)
        {
            field.SetCellPosition(image, new TableLayoutPanelCellPosition(rand.Next(20), rand.Next(20)));
        }
       
        /// <summary>
        /// Ustawia kierunek węża
        /// </summary>
        private void SetDirectionSnake(object sender, KeyEventArgs e)
        {
            switch (e.KeyValue)
            {
                case (int)Keys.Up:
                    direction = 0;
                    break;
                case (int)Keys.Down:
                    direction = 1;
                    break;
                case (int)Keys.Left:
                    direction = 2;
                    break;
                case (int)Keys.Right:
                    direction = 3;
                    break;
            }
        }

        /// <summary>
        /// Zwraca kolumne komórki jesli b = true
        /// Zwraca rząd komórki jesli b = false
        /// </summary>
        private int GetPositionCell(bool b)
        {
            return b ? field.GetColumn(snake1) : field.GetRow(snake1);
        }
        ////////////////////////////////
        /* Prawidłwoe funkcjonalniośc */
        ////////////////////////////////


        ///











    }

    
}
