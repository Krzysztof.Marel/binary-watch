﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BinaryWatch
{
    public partial class Form1 : Form
    {
        string savedTime;

        PictureBox[] second0 = new PictureBox[4];
        PictureBox[] second1 = new PictureBox[3];

        PictureBox[] minute0 = new PictureBox[4];
        PictureBox[] minute1 = new PictureBox[3];

        PictureBox[] hour0 = new PictureBox[4];
        PictureBox[] hour1 = new PictureBox[2];

        public Form1()
        {
            InitializeComponent();
            GroupButtons();
        }

        /// <summary>
        /// Grupuje kolejno kolumny aby wydzielić które kolumny odpowiadają
        /// za cyfre jednostek lub jedności godzin, minut lub sekund.
        /// </summary>
        protected void GroupButtons()
        {
            second0[0] = second00;
            second0[1] = second01;
            second0[2] = second02;
            second0[3] = second03;

            second1[0] = second10;
            second1[1] = second11;
            second1[2] = second12;

            minute0[0] = minute00;
            minute0[1] = minute01;
            minute0[2] = minute02;
            minute0[3] = minute03;

            minute1[0] = minute10;
            minute1[1] = minute11;
            minute1[2] = minute12;

            hour0[0] = hour00;
            hour0[1] = hour01;
            hour0[2] = hour02;
            hour0[3] = hour03;

            hour1[0] = hour10;
            hour1[1] = hour11;
        }

        private void set_timer(object sender, EventArgs e)
        {
            savedTime = DateTime.Now.ToLongTimeString();
            
            timeHour1.Text = savedTime[0].ToString();
            timeHour2.Text = savedTime[1].ToString();
            //colon
            timeMinute1.Text = savedTime[3].ToString();
            timeMinute2.Text = savedTime[4].ToString();
            //colon
            timeSecond1.Text = savedTime[6].ToString();
            timeSecond2.Text = savedTime[7].ToString();
          

            SetPictureBox(hour1, DecimalToBirnary((savedTime[0]), hour1.Length));
            SetPictureBox(hour0, DecimalToBirnary((savedTime[1]), hour0.Length));
            SetPictureBox(minute1, DecimalToBirnary((savedTime[3]), minute1.Length));
            SetPictureBox(minute0, DecimalToBirnary((savedTime[4]), minute0.Length));
            SetPictureBox(second1, DecimalToBirnary((savedTime[6]), second1.Length));
            SetPictureBox(second0, DecimalToBirnary((savedTime[7]), second0.Length));
            myTimer.Start();
        }
      
        private char[] DecimalToBirnary(char letter, int lenght)
        {
            int number = (int)(letter - '0');
            string s = "";

            for(int i = 0; i < lenght; i++)
            {
                s += number > 0 ? (number % 2 == 1 ? "1" : "0") : "0";
                number /= 2;
            }

            return s.ToCharArray();
        }


        private void SetPictureBox(PictureBox[] arrayPicture, char[] binaryNumber)
        {
            for (int i = 0; i< arrayPicture.Length; i++)
            {
                arrayPicture[i].BackColor = binaryNumber[i] == '1' ? Color.Blue : Color.White;
            }
        }
        
    }
    
}
