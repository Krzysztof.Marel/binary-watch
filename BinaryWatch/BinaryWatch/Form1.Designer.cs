﻿namespace BinaryWatch
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.hour11 = new System.Windows.Forms.PictureBox();
            this.hour10 = new System.Windows.Forms.PictureBox();
            this.hour00 = new System.Windows.Forms.PictureBox();
            this.hour01 = new System.Windows.Forms.PictureBox();
            this.hour02 = new System.Windows.Forms.PictureBox();
            this.hour03 = new System.Windows.Forms.PictureBox();
            this.minute03 = new System.Windows.Forms.PictureBox();
            this.minute02 = new System.Windows.Forms.PictureBox();
            this.minute01 = new System.Windows.Forms.PictureBox();
            this.minute00 = new System.Windows.Forms.PictureBox();
            this.minute10 = new System.Windows.Forms.PictureBox();
            this.minute11 = new System.Windows.Forms.PictureBox();
            this.minute12 = new System.Windows.Forms.PictureBox();
            this.second12 = new System.Windows.Forms.PictureBox();
            this.second03 = new System.Windows.Forms.PictureBox();
            this.second02 = new System.Windows.Forms.PictureBox();
            this.second01 = new System.Windows.Forms.PictureBox();
            this.second00 = new System.Windows.Forms.PictureBox();
            this.second10 = new System.Windows.Forms.PictureBox();
            this.second11 = new System.Windows.Forms.PictureBox();
            this.labelHour = new System.Windows.Forms.Label();
            this.labelMinute = new System.Windows.Forms.Label();
            this.labelSecond = new System.Windows.Forms.Label();
            this.colon1 = new System.Windows.Forms.Label();
            this.colon2 = new System.Windows.Forms.Label();
            this.timeHour2 = new System.Windows.Forms.Label();
            this.timeHour1 = new System.Windows.Forms.Label();
            this.timeMinute1 = new System.Windows.Forms.Label();
            this.timeMinute2 = new System.Windows.Forms.Label();
            this.timeSecond1 = new System.Windows.Forms.Label();
            this.timeSecond2 = new System.Windows.Forms.Label();
            this.myTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.hour11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hour10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hour00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hour01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hour02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hour03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.second12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.second03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.second02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.second01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.second00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.second10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.second11)).BeginInit();
            this.SuspendLayout();
            // 
            // hour11
            // 
            this.hour11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hour11.Location = new System.Drawing.Point(13, 105);
            this.hour11.Name = "hour11";
            this.hour11.Size = new System.Drawing.Size(30, 30);
            this.hour11.TabIndex = 0;
            this.hour11.TabStop = false;
            // 
            // hour10
            // 
            this.hour10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hour10.Location = new System.Drawing.Point(13, 141);
            this.hour10.Name = "hour10";
            this.hour10.Size = new System.Drawing.Size(30, 30);
            this.hour10.TabIndex = 1;
            this.hour10.TabStop = false;
            // 
            // hour00
            // 
            this.hour00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hour00.Location = new System.Drawing.Point(49, 141);
            this.hour00.Name = "hour00";
            this.hour00.Size = new System.Drawing.Size(30, 30);
            this.hour00.TabIndex = 2;
            this.hour00.TabStop = false;
            // 
            // hour01
            // 
            this.hour01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hour01.Location = new System.Drawing.Point(49, 105);
            this.hour01.Name = "hour01";
            this.hour01.Size = new System.Drawing.Size(30, 30);
            this.hour01.TabIndex = 3;
            this.hour01.TabStop = false;
            // 
            // hour02
            // 
            this.hour02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hour02.Location = new System.Drawing.Point(49, 68);
            this.hour02.Name = "hour02";
            this.hour02.Size = new System.Drawing.Size(30, 30);
            this.hour02.TabIndex = 4;
            this.hour02.TabStop = false;
            // 
            // hour03
            // 
            this.hour03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hour03.Location = new System.Drawing.Point(49, 32);
            this.hour03.Name = "hour03";
            this.hour03.Size = new System.Drawing.Size(30, 30);
            this.hour03.TabIndex = 5;
            this.hour03.TabStop = false;
            // 
            // minute03
            // 
            this.minute03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minute03.Location = new System.Drawing.Point(131, 32);
            this.minute03.Name = "minute03";
            this.minute03.Size = new System.Drawing.Size(30, 30);
            this.minute03.TabIndex = 12;
            this.minute03.TabStop = false;
            // 
            // minute02
            // 
            this.minute02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minute02.Location = new System.Drawing.Point(131, 68);
            this.minute02.Name = "minute02";
            this.minute02.Size = new System.Drawing.Size(30, 30);
            this.minute02.TabIndex = 11;
            this.minute02.TabStop = false;
            // 
            // minute01
            // 
            this.minute01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minute01.Location = new System.Drawing.Point(131, 105);
            this.minute01.Name = "minute01";
            this.minute01.Size = new System.Drawing.Size(30, 30);
            this.minute01.TabIndex = 10;
            this.minute01.TabStop = false;
            // 
            // minute00
            // 
            this.minute00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minute00.Location = new System.Drawing.Point(131, 141);
            this.minute00.Name = "minute00";
            this.minute00.Size = new System.Drawing.Size(30, 30);
            this.minute00.TabIndex = 9;
            this.minute00.TabStop = false;
            // 
            // minute10
            // 
            this.minute10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minute10.Location = new System.Drawing.Point(95, 141);
            this.minute10.Name = "minute10";
            this.minute10.Size = new System.Drawing.Size(30, 30);
            this.minute10.TabIndex = 8;
            this.minute10.TabStop = false;
            // 
            // minute11
            // 
            this.minute11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minute11.Location = new System.Drawing.Point(95, 105);
            this.minute11.Name = "minute11";
            this.minute11.Size = new System.Drawing.Size(30, 30);
            this.minute11.TabIndex = 7;
            this.minute11.TabStop = false;
            // 
            // minute12
            // 
            this.minute12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minute12.Location = new System.Drawing.Point(95, 68);
            this.minute12.Name = "minute12";
            this.minute12.Size = new System.Drawing.Size(30, 30);
            this.minute12.TabIndex = 13;
            this.minute12.TabStop = false;
            // 
            // second12
            // 
            this.second12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.second12.Location = new System.Drawing.Point(182, 68);
            this.second12.Name = "second12";
            this.second12.Size = new System.Drawing.Size(30, 30);
            this.second12.TabIndex = 20;
            this.second12.TabStop = false;
            // 
            // second03
            // 
            this.second03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.second03.Location = new System.Drawing.Point(218, 32);
            this.second03.Name = "second03";
            this.second03.Size = new System.Drawing.Size(30, 30);
            this.second03.TabIndex = 19;
            this.second03.TabStop = false;
            // 
            // second02
            // 
            this.second02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.second02.Location = new System.Drawing.Point(218, 68);
            this.second02.Name = "second02";
            this.second02.Size = new System.Drawing.Size(30, 30);
            this.second02.TabIndex = 18;
            this.second02.TabStop = false;
            // 
            // second01
            // 
            this.second01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.second01.Location = new System.Drawing.Point(218, 105);
            this.second01.Name = "second01";
            this.second01.Size = new System.Drawing.Size(30, 30);
            this.second01.TabIndex = 17;
            this.second01.TabStop = false;
            // 
            // second00
            // 
            this.second00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.second00.Location = new System.Drawing.Point(218, 141);
            this.second00.Name = "second00";
            this.second00.Size = new System.Drawing.Size(30, 30);
            this.second00.TabIndex = 16;
            this.second00.TabStop = false;
            // 
            // second10
            // 
            this.second10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.second10.Location = new System.Drawing.Point(182, 141);
            this.second10.Name = "second10";
            this.second10.Size = new System.Drawing.Size(30, 30);
            this.second10.TabIndex = 15;
            this.second10.TabStop = false;
            // 
            // second11
            // 
            this.second11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.second11.Location = new System.Drawing.Point(182, 105);
            this.second11.Name = "second11";
            this.second11.Size = new System.Drawing.Size(30, 30);
            this.second11.TabIndex = 14;
            this.second11.TabStop = false;
            // 
            // labelHour
            // 
            this.labelHour.AutoSize = true;
            this.labelHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHour.Location = new System.Drawing.Point(10, 5);
            this.labelHour.Name = "labelHour";
            this.labelHour.Size = new System.Drawing.Size(43, 17);
            this.labelHour.TabIndex = 21;
            this.labelHour.Text = "Hour:";
            // 
            // labelMinute
            // 
            this.labelMinute.AutoSize = true;
            this.labelMinute.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMinute.Location = new System.Drawing.Point(92, 5);
            this.labelMinute.Name = "labelMinute";
            this.labelMinute.Size = new System.Drawing.Size(54, 17);
            this.labelMinute.TabIndex = 22;
            this.labelMinute.Text = "Minute:";
            // 
            // labelSecond
            // 
            this.labelSecond.AutoSize = true;
            this.labelSecond.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSecond.Location = new System.Drawing.Point(179, 5);
            this.labelSecond.Name = "labelSecond";
            this.labelSecond.Size = new System.Drawing.Size(60, 17);
            this.labelSecond.TabIndex = 23;
            this.labelSecond.Text = "Second:";
            // 
            // colon1
            // 
            this.colon1.AutoSize = true;
            this.colon1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.colon1.Location = new System.Drawing.Point(80, 183);
            this.colon1.Name = "colon1";
            this.colon1.Size = new System.Drawing.Size(14, 20);
            this.colon1.TabIndex = 24;
            this.colon1.Text = ":";
            // 
            // colon2
            // 
            this.colon2.AutoSize = true;
            this.colon2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.colon2.Location = new System.Drawing.Point(166, 183);
            this.colon2.Name = "colon2";
            this.colon2.Size = new System.Drawing.Size(14, 20);
            this.colon2.TabIndex = 25;
            this.colon2.Text = ":";
            // 
            // timeHour2
            // 
            this.timeHour2.AutoSize = true;
            this.timeHour2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.timeHour2.Location = new System.Drawing.Point(52, 183);
            this.timeHour2.Name = "timeHour2";
            this.timeHour2.Size = new System.Drawing.Size(27, 20);
            this.timeHour2.TabIndex = 26;
            this.timeHour2.Text = "00";
            // 
            // timeHour1
            // 
            this.timeHour1.AutoSize = true;
            this.timeHour1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.timeHour1.Location = new System.Drawing.Point(16, 183);
            this.timeHour1.Name = "timeHour1";
            this.timeHour1.Size = new System.Drawing.Size(27, 20);
            this.timeHour1.TabIndex = 27;
            this.timeHour1.Text = "00";
            // 
            // timeMinute1
            // 
            this.timeMinute1.AutoSize = true;
            this.timeMinute1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.timeMinute1.Location = new System.Drawing.Point(98, 183);
            this.timeMinute1.Name = "timeMinute1";
            this.timeMinute1.Size = new System.Drawing.Size(27, 20);
            this.timeMinute1.TabIndex = 28;
            this.timeMinute1.Text = "00";
            // 
            // timeMinute2
            // 
            this.timeMinute2.AutoSize = true;
            this.timeMinute2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.timeMinute2.Location = new System.Drawing.Point(134, 183);
            this.timeMinute2.Name = "timeMinute2";
            this.timeMinute2.Size = new System.Drawing.Size(27, 20);
            this.timeMinute2.TabIndex = 29;
            this.timeMinute2.Text = "00";
            // 
            // timeSecond1
            // 
            this.timeSecond1.AutoSize = true;
            this.timeSecond1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.timeSecond1.Location = new System.Drawing.Point(185, 183);
            this.timeSecond1.Name = "timeSecond1";
            this.timeSecond1.Size = new System.Drawing.Size(27, 20);
            this.timeSecond1.TabIndex = 30;
            this.timeSecond1.Text = "00";
            // 
            // timeSecond2
            // 
            this.timeSecond2.AutoSize = true;
            this.timeSecond2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.timeSecond2.Location = new System.Drawing.Point(221, 183);
            this.timeSecond2.Name = "timeSecond2";
            this.timeSecond2.Size = new System.Drawing.Size(27, 20);
            this.timeSecond2.TabIndex = 31;
            this.timeSecond2.Text = "00";
            // 
            // myTimer
            // 
            this.myTimer.Enabled = true;
            this.myTimer.Tick += new System.EventHandler(this.set_timer);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(263, 229);
            this.Controls.Add(this.timeSecond2);
            this.Controls.Add(this.timeSecond1);
            this.Controls.Add(this.timeMinute2);
            this.Controls.Add(this.timeMinute1);
            this.Controls.Add(this.timeHour1);
            this.Controls.Add(this.timeHour2);
            this.Controls.Add(this.colon2);
            this.Controls.Add(this.colon1);
            this.Controls.Add(this.labelSecond);
            this.Controls.Add(this.labelMinute);
            this.Controls.Add(this.labelHour);
            this.Controls.Add(this.second12);
            this.Controls.Add(this.second03);
            this.Controls.Add(this.second02);
            this.Controls.Add(this.second01);
            this.Controls.Add(this.second00);
            this.Controls.Add(this.second10);
            this.Controls.Add(this.second11);
            this.Controls.Add(this.minute12);
            this.Controls.Add(this.minute03);
            this.Controls.Add(this.minute02);
            this.Controls.Add(this.minute01);
            this.Controls.Add(this.minute00);
            this.Controls.Add(this.minute10);
            this.Controls.Add(this.minute11);
            this.Controls.Add(this.hour03);
            this.Controls.Add(this.hour02);
            this.Controls.Add(this.hour01);
            this.Controls.Add(this.hour00);
            this.Controls.Add(this.hour10);
            this.Controls.Add(this.hour11);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.hour11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hour10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hour00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hour01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hour02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hour03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minute12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.second12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.second03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.second02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.second01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.second00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.second10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.second11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox hour11;
        private System.Windows.Forms.PictureBox hour10;
        private System.Windows.Forms.PictureBox hour00;
        private System.Windows.Forms.PictureBox hour01;
        private System.Windows.Forms.PictureBox hour02;
        private System.Windows.Forms.PictureBox hour03;
        private System.Windows.Forms.PictureBox minute03;
        private System.Windows.Forms.PictureBox minute02;
        private System.Windows.Forms.PictureBox minute01;
        private System.Windows.Forms.PictureBox minute00;
        private System.Windows.Forms.PictureBox minute10;
        private System.Windows.Forms.PictureBox minute11;
        private System.Windows.Forms.PictureBox minute12;
        private System.Windows.Forms.PictureBox second12;
        private System.Windows.Forms.PictureBox second03;
        private System.Windows.Forms.PictureBox second02;
        private System.Windows.Forms.PictureBox second01;
        private System.Windows.Forms.PictureBox second00;
        private System.Windows.Forms.PictureBox second10;
        private System.Windows.Forms.PictureBox second11;
        private System.Windows.Forms.Label labelHour;
        private System.Windows.Forms.Label labelMinute;
        private System.Windows.Forms.Label labelSecond;
        private System.Windows.Forms.Label colon1;
        private System.Windows.Forms.Label colon2;
        private System.Windows.Forms.Label timeHour2;
        private System.Windows.Forms.Label timeHour1;
        private System.Windows.Forms.Label timeMinute1;
        private System.Windows.Forms.Label timeMinute2;
        private System.Windows.Forms.Label timeSecond1;
        private System.Windows.Forms.Label timeSecond2;
        private System.Windows.Forms.Timer myTimer;
    }
}

